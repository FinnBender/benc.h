# benc.h

A small and simple header-only benchmarking library.

## How to use

```c
#include "benc.h"
#include <stdint.h>

size_t fib(size_t n) {
  switch (n) {
  case 0: return 0;
  case 1: return 1;
  default: return fib(n-1)+fib(n-2);
  }
}

BENCH_DECL(fib1, fib(1))
BENCH_DECL(fib2, fib(2))
BENCH_DECL(fib5, fib(5))
BENCH_DECL(fib10, fib(10))
BENCH_DECL(fib20, fib(20))
BENCH_DECL(fib40, fib(40))
BENCH_DECL(fib45, fib(45))

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  BENCH_RUN(fib1);
  BENCH_RUN(fib2);
  BENCH_RUN(fib5);
  BENCH_RUN(fib10);
  BENCH_RUN(fib20);
  BENCH_RUN(fib40);
  BENCH_RUN(fib45);
}
```

The program creates the following output:
```
fib1: 3.57 ns ± 23.4 ps (r² = 0.999)
fib2: 8.24 ns ± 30.2 ps (r² = 1.000)
fib5: 36.0 ns ±  176 ps (r² = 0.999)
fib10:  416 ns ±  580 ps (r² = 1.000)
fib20: 52.7 µs ± 82.3 ns (r² = 1.000)
fib40:  798 ms ± 2.56 ms (r² = 1.000)
fib45: 8.87  s ±   NaN   (r² = 1.000)
```

The number after the ± is twice the standard deviation. This means
that in 95% of all cases, the time does not deviate more than this
number from the mean.
