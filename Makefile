example: example.o TAGS
	cc example.o -lm -o example

example.o: example.c benc.h
	cc -c example.c -o example.o -Wall -Wextra -Wpedantic

TAGS: benc.h
	etags benc.h
