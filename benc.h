#ifndef BENC_H
#define BENC_H

#include <malloc.h>
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <time.h>

#if defined(__GNUC__)
#define BENCH_INLINE __attribute__((always_inline)) inline
#else
#define BENCH_INLINE inline
#endif

#define BENCH_DECL(name, value)               \
  void bench_autogen_##name () {              \
    value;                                    \
  }

#define BENCH_RUN(name) \
  benchmark(#name, &bench_autogen_##name)


typedef struct {
  char const *name;
  void (*body)(void);
  double max_wall_time;
  double progression;
} Benchmark;

typedef struct {
  char const *name;
  size_t sample_size;
  double *run_counts;
  double *cpu_times;
  double total_wall_time;
} Measurement;

typedef struct {
  char const *name;
  double expected_cpu_time;
  double standard_deviation;
  double correlation;
  double total_wall_time;
} Result;

static BENCH_INLINE Measurement measure(Benchmark const benchmark) {
  size_t sample_size = 1;
  double *run_counts = (double *)malloc(sizeof(double));
  double *cpu_times = (double *)malloc(sizeof(double));
  run_counts[0] = 0;
  cpu_times[0] = 0;

  double run_count = 1.;

  time_t const wall_time_beg = time(NULL);
  time_t wall_time_end;
  double total_wall_time = 0;

  while (total_wall_time < benchmark.max_wall_time) {
    clock_t beg = clock();

    for (uint64_t i = 1; i <= (uint64_t)run_count; ++i) {
      benchmark.body();
    }

    clock_t end = clock();

    run_counts =
        (double *)reallocarray(run_counts, sample_size + 1, sizeof(double));
    run_counts[sample_size] = (double)run_count;
    cpu_times =
        (double *)reallocarray(cpu_times, sample_size + 1, sizeof(double));
    cpu_times[sample_size] = (double)(end - beg) / CLOCKS_PER_SEC;
    sample_size += 1;

    run_count = ceil(run_count * benchmark.progression);

    time(&wall_time_end);
    total_wall_time = difftime(wall_time_end, wall_time_beg);
  }

  Measurement measurement;
  measurement.name = benchmark.name;
  measurement.sample_size = sample_size;
  measurement.run_counts = run_counts;
  measurement.cpu_times = cpu_times;
  measurement.total_wall_time = total_wall_time;

  return measurement;
}

static double mean(double *values, size_t n) {
  double sum = 0;
  for (size_t i = 0; i < n; ++i) {
    sum += values[i];
  }
  return sum / (double)n;
}

static inline Result analyze_ols(Measurement const measurement) {
  double mean_run_count = mean(measurement.run_counts, measurement.sample_size);
  double mean_cpu_time = mean(measurement.cpu_times, measurement.sample_size);
  double covariance = 0;
  double run_count_variance = 0;
  double cpu_time_variance = 0;

  for (size_t i = 0; i < measurement.sample_size; ++i) {
    double run_count_diff = measurement.run_counts[i] - mean_run_count;
    double cpu_time_diff = measurement.cpu_times[i] - mean_cpu_time;
    covariance += run_count_diff * cpu_time_diff;
    run_count_variance += run_count_diff * run_count_diff;
    cpu_time_variance += cpu_time_diff * cpu_time_diff;
  }

  double slope = covariance / run_count_variance;
  double intercept = mean_cpu_time - slope * mean_run_count;
  double pearson_correlation =
      covariance / sqrt(run_count_variance * cpu_time_variance);

  double cpu_time_error_acc = 0.;
  for (size_t i = 0; i < measurement.sample_size; ++i) {
    double run_count = measurement.run_counts[i];
    double predicted_cpu_time = intercept + slope * run_count;
    double cpu_time_error = measurement.cpu_times[i] - predicted_cpu_time;
    cpu_time_error_acc += cpu_time_error * cpu_time_error;
  }
  double standard_error =
      sqrt(cpu_time_error_acc /
           ((measurement.sample_size - 2) * run_count_variance));

  Result result;
  result.name = measurement.name;
  result.expected_cpu_time = slope;
  result.standard_deviation = 1.959963984540 * standard_error;
  result.correlation = pearson_correlation * pearson_correlation;
  result.total_wall_time = measurement.total_wall_time;

  return result;
}

static inline void fprint_time(FILE *stream, double time) {
  if (isnan(time)) {
    fprintf(stream, "  NaN  ");
    return;
  }
  char const *units[5] = {" s", "ms", "µs", "ns", "ps"};
  size_t unit_index = 0;
  while (unit_index < 4 && time < 1.) {
    unit_index++;
    time *= 1000.;
  }

  if (time < 10) {
    fprintf(stream, "%.2f %s", time, units[unit_index]);
  } else if (time < 100) {
    fprintf(stream, "%.1f %s", time, units[unit_index]);
  } else {
    fprintf(stream, " %.0f %s", time, units[unit_index]);
  }
}

static BENCH_INLINE void benchmark(char const *name, void (*func)(void)) {
  Benchmark benchmark;
  benchmark.name = name;
  benchmark.body = func;
  benchmark.max_wall_time = 1.0;
  benchmark.progression = 1.05;

  fprintf(stdout, "%s: ", benchmark.name);
  fflush(stdout);
  Measurement measurement = measure(benchmark);
  Result result = analyze_ols(measurement);

  if (isnan(result.expected_cpu_time)) {
    fprintf(stdout, "<noop>\n");
  } else {
    fprint_time(stdout, result.expected_cpu_time);
    fprintf(stdout, " ± ");
    fprint_time(stdout, 2 * result.standard_deviation);
    fprintf(stdout, " (r² = %.03f)\n", pow(result.correlation, 2));
  }
}

#endif
