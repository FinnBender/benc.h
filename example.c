#include "benc.h"
#include <stdint.h>

size_t fib(size_t n) {
  switch (n) {
  case 0: return 0;
  case 1: return 1;
  default: return fib(n-1)+fib(n-2);
  }
}

BENCH_DECL(fib1, fib(1))
BENCH_DECL(fib2, fib(2))
BENCH_DECL(fib5, fib(5))
BENCH_DECL(fib10, fib(10))
BENCH_DECL(fib20, fib(20))
BENCH_DECL(fib40, fib(40))
BENCH_DECL(fib45, fib(45))

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  BENCH_RUN(fib1);
  BENCH_RUN(fib2);
  BENCH_RUN(fib5);
  BENCH_RUN(fib10);
  BENCH_RUN(fib20);
  BENCH_RUN(fib40);
  BENCH_RUN(fib45);
}
